package prassee.plass

import com.sun.net.httpserver.HttpServer
import java.net.InetSocketAddress
import java.net.InetAddress
import com.sun.net.httpserver.HttpHandler
import com.sun.net.httpserver.HttpExchange
import scala.collection.mutable.HashMap
import scala.util.matching.Regex
import scala.util.parsing.combinator._
import scala.util.matching.Regex
import scala.collection.mutable.ArrayBuffer

private object Routing {

  def getRegexForRoute(route: String): (String, List[String], Regex) = {
    val x = RouteDSL.parse(RouteDSL.dynamicURL, route).get
    val y = x._1.mkString + x._2.map(x => "/(\\w)+").mkString
    (x._1.mkString, x._2.map(x => x.replace("/:", "")), y.r)
  }

  def getRouteForRegex(data: String,
    routes: List[String]): Option[((String, List[String], Regex), Map[String, String])] = {

    case class RouteDefinition(routeDef: String, routeRegx: Regex,
      routePrefix: String, routeSuffixes: List[String])

    val routesList = routes.map { x =>
      val tup = getRegexForRoute(x)
      RouteDefinition(x, tup._3, tup._1, tup._2.map { x => x.replace("/:", "") })
    }
    val filteredRoute =
      routesList.filter(x => data.startsWith(x.routePrefix)).headOption

    filteredRoute match {
      case Some(routeDef) =>
        val strA = RouteDSL.resolveValue(routeDef.routeRegx, data)
        val diff = strA.head.diff(routeDef.routePrefix.toList).split("/")
          .filter(x => x.nonEmpty).toList
        Option(((routeDef.routePrefix, routeDef.routeSuffixes, routeDef.routeRegx),
          routeDef.routeSuffixes.zip(diff).map(f => f._1 -> f._2).toMap))
      case None => None
    }
  }

}

private object RouteDSL extends JavaTokenParsers {
  def dynamicURL = rep("""/\w+""".r) ~ rep("""/:\w+""".r)
  def resolveValue(regex: Regex, rte: String) = parse(rep(regex) ~ "", rte).get._1
}

abstract class PlassBaseServer(port: Int) extends HttpHandler {

  private val address = new InetSocketAddress("127.0.0.1", port)
  private lazy val server = HttpServer.create(address, port)
  server.createContext("/", this)

  protected val mapping = HashMap[String, HttpExchange => String]()
  protected val mappingr = HashMap[(String, List[String], Regex), HttpExchange => String]()
  protected val routesLis = ArrayBuffer[String]()
  protected var params = Map[String, String]()

  def get(route: String)(hdle: (HttpExchange) => String) = {
    if (route.contains(":")) {
      mappingr.+=(Routing.getRegexForRoute(route) -> hdle)
      routesLis.+=:(route)
    } else {
      mapping.+=(route -> hdle)
    }
  }

  def handle(xchange: HttpExchange) = {
    val reqURI = xchange.getRequestURI.getPath
    val x = Routing.getRouteForRegex(reqURI, routesLis.toList)
    val resp = x match {
      case Some((map, b)) =>
        params = x.get._2
        val entry = mappingr.filter(x => x._1._1 == map._1).head
        entry._2(xchange)
      case _ =>
        mapping.get(reqURI).get(xchange)
    }
    xchange.sendResponseHeaders(200, resp.size)
    xchange.getResponseBody.write(resp.getBytes)
    xchange.getResponseBody.write("\r\n\r\n".getBytes)
    xchange.getResponseBody.close()
    xchange.close()
  }

  def start = server.start()

}
