package prassee.plass

/*
 * 
 */
object MicroHttpServer extends App {
  val blog = new MyBlogServer(8000)
  blog.start
}

/*
 * 
 */
class MyBlogServer(port: Int) extends PlassBaseServer(port) {

  get("/") { xchange =>
    "tis works"
  }

  get("/micro") { xchange =>
    "micor"
  }

  get("/api/:page") { xchange =>
    println()
    "matches /show/:page"
  }

  get("/show/api/:page") { xchange =>
    s"matches /show/api/${params("page")}"
  }

  get("/update/api/:page") { xchange =>
    s"matches /update/api/${params("page")}"
  }

  get("/render/api/:page/:json") { xchange =>
    s"matches /render/api/${params("page")}/${params("json")}"
  }
}