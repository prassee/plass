# Plass
Plain Simple Scala Web Server

A simple light-weight HTTP server handler written in Scala. 
Plass uses the webserver which comes along with JDK, 
hence it does not depend on any other dependency. 
This pretty much looks like Scalatra , Finatra and the likes.

## Current Version 0.0.2 

## Using Plass 

in build.sbt 
	
	libraryDependencies ++= Seq("prassee" %% "plass" % "0.0.2")

	resolvers ++= Seq("moma" at "https://github.com/prassee/moma/raw/master/snapshots")
	

## How to write a Handler

   	class MyBlogServer(port: Int) extends PlassBaseServer(port) {
	      get("/") { xchange =>
	       	"tis works"
	      }
	
	      get("/micro") { xchange =>
	       	"micor"
	      }
	
	     get("/api/:page") { xchange =>
		      println()
		      "matches /show/:page"
	     }
	
	     get("/show/api/:page") { xchange =>
	      	s"matches /show/api/${params("page")}"
	     }
    }  

## Using the Handler

	object MicroHttpServer extends App {
  		val blog = new MyBlogServer(8000)
  		blog.start
	}

## For what I can use Plass 

* Writing microservice
* Simple HTTP handler 
* Develop your applicaiton specific handlers 
 
 
